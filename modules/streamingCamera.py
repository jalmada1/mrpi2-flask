import picamera
from .streamingOutput import StreamingOutput
import logging
import threading


BLACK_AND_WHITE = (128, 128)
LOOP_TIME = 60

class StreamingCamera:

    def __init__(self, startRecording):
        self.camera = picamera.PiCamera(resolution='720x540', framerate=20)
        self.loopstream = picamera.PiCameraCircularIO(self.camera, seconds=60)  
        self.output = StreamingOutput()
        if(startRecording):
            self.camera.start_recording(self.loopstream, format='h264')
            self.camera.start_recording(self.output, format='mjpeg', splitter_port=2)
            self.SaveVideo()

    def Flip(self, flipX, flipY):
        self.camera.vflip = flipX
        self.camera.hflip = flipY
        
    def TakePicture(self, fileName):
        self.camera.capture(f'./captures/{fileName}.jpg')

    def Stream(self):
        try:
            while True:
                with self.output.condition:
                    self.output.condition.wait()
                    frame = self.output.frame

                    yield (b'--frame\r\n'
                            b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')
        except Exception as e:
            logging.warning(e)
    
    def Stop(self):
        self.camera.stop_recording(splitter_port=2)
        self.camera.stop_recording()
        self.camera.close()

    def SetEffects(self, uv):
        self.camera.color_effects = uv

    def SaveVideo(self):
        threading.Timer(LOOP_TIME, self.SaveVideo).start()
        print("Saving 60 sec Video")
        self.camera.request_key_frame()
        self.loopstream.copy_to('lastmin.h264')
        self.loopstream.clear()

